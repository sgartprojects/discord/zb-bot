import feedparser

from constants.secrets import pfadialarm_rss_feed_url
from re import compile, findall


async def fetch_alarm():
    return feedparser.parse(pfadialarm_rss_feed_url)["entries"]


async def fetch_clean_alarm():
    pfadialarm_rss = await fetch_alarm()
    pfadialarm = []
    for activity in pfadialarm_rss:
        title = activity['title']
        info = activity['summary'].split('-:-')
        date = info[0]
        time = info[1]
        location = info[2]
        address = info[3]
        town = info[4]

        categories = findall(">Aktivitäten (.*)</a>", info[5])
        if len(categories) == 1:
            category = categories[0]
        elif 'Rudel Kaihua' in categories and \
                    'Rudel Inferno' in categories and \
                    'Rudel Izumi' in categories and \
                    'Rudel Fuchur' in categories and \
                    len(categories) == 4:
            category = 'Meute Momo'
        else:
            category = ''
            for cat in categories:
                if categories.index(cat) != 0:
                    category += ", "
                category += cat

        description = compile(r'<[^>]+>').sub('', info[6]).replace('&#160;', '')

        new_activity = {
            'title': title,
            'category': category,
            'description': description,
            'date': date,
            'time': time,
            'location': location,
            'address': address,
            'town': town
        }
        pfadialarm.append(new_activity)

    return pfadialarm
