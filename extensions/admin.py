import discord
from discord import SlashCommandGroup

from discord.ext import commands
from helpers.logger import log


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(
        name='sync',
        description='Synchronisier alli Befehl'
    )
    @commands.is_owner()
    async def sync(self, ctx):
        await self.bot.sync_commands()
        await ctx.respond('Done')

    extension = SlashCommandGroup('extensions', 'Befehl für sModulmanagement')

    @extension.command(
        name='load',
        description='Lad e extension'
    )
    @commands.guild_only()
    @commands.is_owner()
    async def load(self, ctx, extension):
        extension = f'extensions.{extension}'
        if extension in self.bot.extensions:
            await ctx.respond(f'Extension "{extension}" is already loaded')
        else:
            log(f'Loading extension "{extension}"...', 'info')
            try:
                self.bot.load_extension(extension)
                log(f'Loading of extension "{extension}" done: Loaded by admin', 'success')
                await ctx.respond(f'Extension "{extension}" loaded successfully')
            except Exception as e:
                log(f'Error while loading extension "{extension}"', 'error', e)
                await ctx.respond(f'Extension "{extension}" could not be loaded')

    @extension.command(
        name='unload',
        description='Entlad e extension'
    )
    @commands.guild_only()
    @commands.is_owner()
    async def unload(self, ctx, extension):
        extension = f'extensions.{extension}'
        if extension not in self.bot.extensions:
            await ctx.respond(f'Extension "{extension}" is not loaded')
        else:
            log(f'Unloading extension "{extension}"...', 'info')
            try:
                self.bot.unload_extension(extension)
                log(f'Unoading of extension "{extension}" done', 'success')
                await ctx.respond(f'Extension "{extension}" unloaded successfully')
            except Exception as e:
                log(f'Error while unloading extension "{extension}"', 'error', e)
                await ctx.respond(f'Extension "{extension}" could not be unloaded')

    @extension.command(
        name='list',
        description='List alli extensions uuf'
    )
    @commands.guild_only()
    @commands.is_owner()
    async def list(self, ctx):
        await ctx.respond('Extensions:\n- ' + '\n- '.join(self.bot.extensions).replace('extensions.', ''))



def setup(bot):
    bot.add_cog(Admin(bot))
