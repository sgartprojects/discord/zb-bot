import discord

from datetime import date
from discord.ext import commands
from discord import SlashCommandGroup, ApplicationContext

from helpers.logger import log
from constants.constants import \
    admin_id, al_id, stalei_id, rulei_id, mitleitung_id
from helpers.database import read, save
from dateutil.parser import parse
from components.modals.camp_modals import NewCampModal, DeleteCampView


class Camps(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    camps = SlashCommandGroup('camps', 'Befehl für sLagermanagement')

    @camps.command(
        name='new',
        description='Erstell es neus Lager'
    )
    @commands.has_any_role(admin_id, al_id, stalei_id, rulei_id)
    @commands.guild_only()
    async def new(self, ctx: ApplicationContext):

        modal = NewCampModal(title="Lager erstelle")

        await ctx.send_modal(modal)

    edit = camps.create_subgroup('edit', 'Befehl zum Lager bearbeite')

    @edit.command(
        name='name',
        description='Bearbeit de Name vomene Lager'
    )
    @commands.has_any_role(admin_id, al_id, stalei_id, rulei_id)
    @commands.guild_only()
    async def name(self, ctx, oldname: str, newname: str):
        data = read('camps.json', './data')
        camps = data['camps']
        for camp in camps:
            if camp["name"] == oldname:
                camp["name"] = newname
        save(data, 'camps.json', './data')
        await ctx.respond(f'{oldname} isch zu {newname} umbenennt worde')

    @edit.command(
        name='startdate',
        description='Bearbeit sStartdatum vomene Lager'
    )
    async def startdate(self, ctx, name: str, newdate: str):
        try:
            start_date_date = parse(timestr=newdate).date()
        except Exception as e:
            log(f'Error while converting "{newdate}" to date', 'error', e, True)
            raise commands.BadArgument(message='End date has to be a valid date (YYYY-MM-DD)')

        data = read('camps.json', './data')
        camps = data['camps']
        for camp in camps:
            if camp["name"] == name:
                camp["start_date"] = start_date_date.isoformat()
        save(data, 'camps.json', './data')
        await ctx.respond(f'{name} findet jetzt ab {start_date_date.strftime("%d.%m.%Y")} statt')

    @edit.command(
        name='enddate',
        description='Bearbeit sEnddatum vomene Lager'
    )
    async def enddate(self, ctx, name: str, newdate: str):
        try:
            end_date_date = parse(timestr=newdate).date()
        except Exception as e:
            log(f'Error while converting "{newdate}" to date', 'error', e, True)
            raise commands.BadArgument(message='End date has to be a valid date (YYYY-MM-DD)')

        data = read('camps.json', './data')
        camps = data['camps']
        for camp in camps:
            if camp["name"] == name:
                camp["end_date"] = end_date_date.isoformat()
        save(data, 'camps.json', './data')
        await ctx.respond(f'{name} findet jetzt ab {end_date_date.strftime("%d.%m.%Y")} statt')

    @camps.command(
        name='delete',
        description='Entfern es Lager us de Lischte'
    )
    @commands.has_any_role(admin_id, al_id, stalei_id, rulei_id)
    @commands.guild_only()
    async def delete(self, ctx):
        delete_camp_view = DeleteCampView()
        await delete_camp_view.refresh_data()
        await ctx.respond(view=delete_camp_view)

    list = camps.create_subgroup(
        name='list',
        description='Befehl zum Lager uuflischte'
    )

    @list.command(
        name='current',
        description='Lischt alli zuekünftige und laufende Lager uuf'
    )
    @commands.has_any_role(admin_id, al_id, stalei_id, rulei_id, mitleitung_id)
    @commands.guild_only()
    async def current(self, ctx):
        camps = read('camps.json', './data')['camps']

        embed = discord.Embed(
            title='Aktivi Lager',
            description='Zuekünftigi oder aktuelli Lager',
            color=ctx.author.color
        )
        for camp in camps:
            end_date = date.fromisoformat(camp['end_date'])
            if end_date >= date.today():
                start_date_string = date.fromisoformat(camp['start_date']).strftime('%d.%m.%Y')
                end_date_string = end_date.strftime('%d.%m.%Y')
                embed.add_field(
                    name=camp['name'],
                    value=f'**Startdatum:** {start_date_string}\n'
                          f'**Enddatum:** {end_date_string}\n'
                )
        embed.set_footer(
            text=f'Uusgführt vo {ctx.author}',
            icon_url=ctx.author.display_avatar
        )
        await ctx.respond(embed=embed)

    @list.command(
        name='all',
        description='Lischt alli iitreite Lager uuf'
    )
    @commands.has_any_role(admin_id, al_id, stalei_id, rulei_id, mitleitung_id)
    @commands.guild_only()
    async def all(self, ctx):
        camps = read('camps.json', './data')['camps']

        embed = discord.Embed(
            title='Alli Lager',
            description='Alli registrierte Lager',
            color=ctx.author.color
        )
        if len(camps) == 0:
            embed.description = 'Kei registrierti Lager'
        for camp in camps:
            start_date_string = date.fromisoformat(camp['start_date']).strftime('%d.%m.%Y')
            end_date_string = date.fromisoformat(camp['end_date']).strftime('%d.%m.%Y')
            embed.add_field(
                name=camp['name'],
                value=f'**Startdatum:** {start_date_string}\n'
                      f'**Enddatum:** {end_date_string}\n'
            )
        embed.set_footer(
            text=f'Uusgführt vo {ctx.author}',
            icon_url=ctx.author.display_avatar
        )
        await ctx.respond(embed=embed)

    @list.command(
        name='passed',
        description='Lischt alli vergangene Lager uuf'
    )
    async def passed(self, ctx):
        camps = read('camps.json', './data')['camps']

        embed = discord.Embed(
            title='Vergangeni Lager',
            description='Lager us de vergangeheit',
            color=ctx.author.color
        )
        for camp in camps:
            end_date = date.fromisoformat(camp['end_date'])
            if end_date < date.today():
                start_date_string = date.fromisoformat(camp['start_date']).strftime('%d.%m.%Y')
                end_date_string = end_date.strftime('%d.%m.%Y')
                embed.add_field(
                    name=camp['name'],
                    value=f'**Startdatum:** {start_date_string}\n'
                          f'**Enddatum:** {end_date_string}\n'
                )
        embed.set_footer(
            text=f'Uusgführt vo {ctx.author}',
            icon_url=ctx.author.display_avatar
        )
        await ctx.respond(embed=embed)


def setup(bot):
    bot.add_cog(Camps(bot))
