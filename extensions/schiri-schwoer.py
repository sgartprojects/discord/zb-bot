import discord
import requests

from discord.ext import commands
from constants.secrets import schiri_webhook_url
from helpers.logger import log


class SchiriSchwoer(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, msg):
        if (not isinstance(msg.channel, discord.DMChannel)) and msg.channel.name == "schiri-schwör" and not msg.author.bot:
            data = {
                'username': f'{msg.author.display_name}',
                'avatar_url': str(msg.author.display_avatar),
                'content': f"Schiri schwör, {msg.content}"
            }
            await msg.delete()
            res = requests.post(schiri_webhook_url, data)
            if res.status_code != 204:
                log(f'Webhook request failed with code {res.status_code}', 'error', res.text, True)


def setup(bot):
    bot.add_cog(SchiriSchwoer(bot))
