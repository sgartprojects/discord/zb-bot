import discord

from asyncio import TimeoutError
from discord.ext import commands, tasks
from os import listdir
from constants.config import env
from constants.secrets import token
from constants.constants import home_guild_id, camp_chat_cat_id, camp_voice_cat_id, zb_logo_url
from helpers.logger import log
from datetime import datetime, date
from helpers.database import read
from dateutil.relativedelta import relativedelta

log('Starting up...', 'info')

log('Setting prefix...', 'info')
prefix = read('config.json', './data')["prefix"][env]
log(f'Prefix set to {prefix}', 'success')

intents = discord.Intents.all()
bot = commands.Bot(command_prefix=prefix, case_insensitive=True, intents=intents)
bot.remove_command('help')

log('Loading extensions...', 'info')
for filename in listdir('./extensions'):
    if filename.endswith('.py'):
        log(f'Loading extension "{filename[:-3]}"...', 'info')
        try:
            bot.load_extension(f'extensions.{filename[:-3]}')
            log(f'Loading of extension "{filename[:-3]}" done', 'success')
        except Exception as e:
            log(f'Error while loading extension "{filename[:-3]}"', 'error', e, True)

@bot.event
async def on_connect():
    log('Syncing commands...', 'info');
    try:
        await bot.sync_commands()
        log('Commands synced sucessfuly', 'success')
    except Exception as sync_error:
        log(f'Error while syncing commands', 'error', sync_error, True)

@bot.event
async def on_ready():
    log('Starting tasks', 'info', None, False)
    manage_presence.start()
    manage_camp_channels.start()
    log('Startup successful', 'success', None, env=='prod')


@tasks.loop(minutes=1)
async def manage_presence():
    log('Executing task "Manage Presence"', 'debug', None, False)
    now = datetime.now()

    if now.weekday() == 5 and 14 <= now.hour <= 17:
        await bot.change_presence(
            activity=discord.Activity(
                name=f'a de Aktivität',
                type=discord.ActivityType.playing,
                url='https://pfadi-zueriberg.ch',
                state='Hät Freud a de Aktivität',
                assets=dict(
                    large_image=zb_logo_url,
                    large_text='Züriberg Logo'
                )
            )
        )
    elif now.weekday() == 1 and 19 <= now.hour <= 22:
        await bot.change_presence(
            activity=discord.Activity(
                name=f'bim Höck zue',
                type=discord.ActivityType.listening,
                url='https://pfadi-zueriberg.ch',
                state='Plant die nächst Aktivität',
                assets=dict(
                    large_image=zb_logo_url,
                    large_text='Züriberg Logo'
                )
            )
        )
    elif now.day == 9 and now.month == 11 and 10 <= now.hour <= 16:
        await bot.change_presence(
            activity=discord.Activity(
                type=discord.ActivityType.custom,
                url='https://pfadi-zueriberg.ch',
                state='Plant snächste Jahr - Happy Planigstag!',
            )
        )
    else:
        await bot.change_presence(
            activity=discord.Activity(
                name=f'ufd Ziit: {now.strftime("%H:%M")}',
                type=discord.ActivityType.watching,
                url='https://pfadi-zueriberg.ch',
                state='Luegt ufd Uhr',
                assets=dict(
                    large_image=zb_logo_url,
                    large_text='Züriberg Logo'
                )
            )
        )


@tasks.loop(hours=12)
async def manage_camp_channels():
    log('Executing task "Manage Camp Channels"', 'debug', False, None)
    camps = read('camps.json', './data')["camps"]
    guild = discord.utils.get(bot.guilds, id=home_guild_id)
    camp_chat_cat = discord.utils.get(guild.categories, id=camp_chat_cat_id)
    camp_voice_cat = discord.utils.get(guild.categories, id=camp_voice_cat_id)
    for camp in camps:
        camp_start_date = date.fromisoformat(camp['start_date'])
        camp_end_date = date.fromisoformat(camp['end_date'])
        if camp_start_date - relativedelta(months=12) <= date.today() <= camp_end_date + relativedelta(
                months=2):
            if discord.utils.get(camp_chat_cat.text_channels,
                                 name=camp['name'].lower().replace(' ', '-')) is None:
                await camp_chat_cat.create_text_channel(camp['name'])
                log(f'Created text channel for camp "{camp["name"]}": In date range', 'info', None, True)
            else:
                log(f'Skipped creation of text channel for camp "{camp["name"]}": Already exists', 'info')

            if discord.utils.get(camp_voice_cat.voice_channels, name=camp['name']) is None:
                await camp_voice_cat.create_voice_channel(camp['name'])
                log(f'Created voice channel for camp "{camp["name"]}": In date range', 'info', None, True)
            else:
                log(f'Skipped creation of voice channel for camp "{camp["name"]}": Already exists', 'info')
        else:
            chat = discord.utils.get(camp_chat_cat.text_channels, name=camp['name'].lower().replace(' ', '-'))
            voice = discord.utils.get(camp_voice_cat.voice_channels, name=camp['name'])
            if chat is not None:
                await chat.delete()
                log(f'Deleted text channel for camp "{camp["name"]}": Out of date range', 'info', None, True)
            else:
                log(f'Skipped deletion of text channel for camp "{camp["name"]}": Doesn\'t exist', 'info')

            if voice is not None:
                await voice.delete()
                log(f'Deleted voice channel for camp "{camp["name"]}: Out of date range"', 'info', None, True)
            else:
                log(f'Skipped deletion of voice channel for camp "{camp["name"]}": Doesn\'t exist', 'info')


@bot.event
async def on_command_error(ctx, error):
    thread = ctx.message.channel.type == discord.ChannelType.public_thread
    message = await get_message_and_log(ctx.command, error)
    if thread:
        await ctx.channel.send(message)
    else:
        await ctx.reply(message)


@bot.event
async def on_application_command_error(ctx, error):
    parsed_error = getattr(error, 'original', error)
    message = await get_message_and_log(ctx.command, parsed_error)
    await ctx.respond(message)


async def get_message_and_log(command, error):
    if command is None:
        message = "De Befehl kenn ich nöd! Hesch en richtig gschribe?"
        log(f'Error while executing command', 'error', error, False)
    elif isinstance(error, commands.CommandNotFound):
        message = 'De Befehl kenn ich nöd! Hesch en richtig gschribe?'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.MissingRequiredArgument):
        message = f'Mir fehled benötigti Informatione! Bitte nomal probiere\n```{error}```'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.BadArgument):
        message = f'Falschi Informatione! Bitte nomal probiere\n```{error}```'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.BotMissingPermissions):
        message = 'Mir fehled benötigti Berechtigunge! Beschwer dich bim Beetle'
        log(f'Error while executing command "{command.name}"', 'error', error, True)
    elif isinstance(error, commands.MissingAnyRole):
        message = 'Dir fehled benötigti Berechtigunge!'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.CommandOnCooldown):
        message = 'De Befehl isch grad no am abchüele :)! Bitte später nomal probiere'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.DisabledCommand):
        message = 'De Befehl isch im Moment deaktiviert!'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.NoPrivateMessage):
        message = 'De Befehl cha nur uf em Server usgfüehrt werde!'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, commands.MissingPermissions):
        message = f'Fehlendi Berechtigunge {str(error.missing_permissions).replace("[", "").replace("]", "")}'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, TimeoutError):
        message = 'Timed out. Bitte nomal probiere'
        log(f'Error while executing command "{command.name}"', 'error', error, False)
    elif isinstance(error, OSError):
        message = error.args[0]
        log(f'Error while executing command "{command.name}"', 'error', error, True)
    else:
        message = 'Unbekannte Fehler! Meld dich bim Beetle'
        log(f'Error while executing command "{command.name}"', 'error', error, True)

    return message


bot.run(token)
