import discord

from discord.ext import commands
from helpers.logger import log
from constants.constants import admin_id, al_id, stalei_id, rulei_id, mitleitung_id


class Tempvoice(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        channel_name = f"{member.display_name} - TempVoice"
        if before.channel is not None:
            if before.channel.name == channel_name:
                await before.channel.delete()
        if after.channel is not None:
            if after.channel.name == "Tempvoice":
                pre_channel = discord.utils.get(after.channel.guild.channels, name=channel_name)
                channel = pre_channel
                if pre_channel is None:
                    channel = await after.channel.category.create_voice_channel(channel_name)
                    # everyone = discord.utils.get(channel.guild.roles, name="@everyone")
                    # await channel.set_permissions(everyone, connect=False, speak=True)
                    # await channel.set_permissions(member,
                    #                               connect=True,
                    #                               speak=True,
                    #                               mute_members=True,
                    #                               deafen_members=True,
                    #                               use_voice_activation=True)

                await member.move_to(channel)

    @commands.group(
        name="tempvoice",
        aliases=["tv"]
    )
    @commands.guild_only()
    async def tempvoice(self, ctx):
        if ctx.invoked_subcommand is None:
            raise commands.CommandNotFound


def setup(bot):
    bot.add_cog(Tempvoice(bot))
