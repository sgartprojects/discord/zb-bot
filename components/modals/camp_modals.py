import discord

from discord.ui import Modal, View, InputText, select
from discord import Interaction, SelectOption
from discord.ext import commands
from dateutil.relativedelta import relativedelta
from helpers.logger import log
from helpers.database import read, save
from dateutil.parser import parse
from datetime import date
from constants.constants import camp_chat_cat_id, camp_voice_cat_id


class NewCampModal(Modal):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.add_item(InputText(label="Lagername"))
        self.add_item(InputText(label="Startdatum (YYYY-MM-DD)"))
        self.add_item(InputText(label="Enddatum (YYYY-MM-DD"))

    async def callback(self, interaction: Interaction):
        data = read('camps.json', './data')
        camps = data['camps']

        name = self.children[0].value
        startdatum = self.children[1].value
        enddatum = self.children[2].value

        try:
            start_date_date = parse(timestr=startdatum).date()
        except Exception as e:
            log(f'Error while converting "{startdatum}" to date', 'error', e, True)
            raise commands.BadArgument(message='Start date has to be a valid date (YYYY-MM-DD)')

        try:
            end_date_date = parse(timestr=enddatum).date()
        except Exception as e:
            log(f'Error while converting "{enddatum}" to date', 'error', e, True)
            raise commands.BadArgument(message='End date has to be a valid date (YYYY-MM-DD)')

        for camp in camps:
            if camp["name"] == name:
                raise commands.BadArgument(f'A camp with the name "{name}" already exists!')

        camp = {
            'name': name,
            'start_date': start_date_date.isoformat(),
            'end_date': end_date_date.isoformat()
        }
        camps.append(camp)
        save(data, 'camps.json', './data')

        embed_start_date_string = start_date_date.strftime('%d.%m.%Y')
        embed_end_date_string = end_date_date.strftime('%d.%m.%Y')
        embed = discord.Embed(
            title='Lager erstellt',
            description=f'List alli Lager uuf mit ``/camps list all``',
        )
        embed.add_field(
            name='Name',
            value=name
        )
        embed.add_field(
            name='Startdatum',
            value=embed_start_date_string
        )
        embed.add_field(
            name='Enddatum',
            value=embed_end_date_string
        )
        await interaction.response.send_message(embed=embed)

        if start_date_date - relativedelta(months=8) <= date.today() <= end_date_date + relativedelta(months=1):
            guild = interaction.guild
            camp_chat_cat = discord.utils.get(guild.categories, id=camp_chat_cat_id)
            camp_voice_cat = discord.utils.get(guild.categories, id=camp_voice_cat_id)
            await camp_chat_cat.create_text_channel(name)
            log(f'Created text channel for camp "{camp["name"]}": Camp created by user', 'info', None, True)
            await camp_voice_cat.create_voice_channel(name)
            log(f'Created voice channel for camp "{camp["name"]}": Camp created by user', 'info', None, True)


class DeleteCampView(View):
    data = read('camps.json', './data')
    camps = data['camps']
    selectOptions = []

    for camp in camps:
        selectOptions.append(SelectOption(
            label=camp["name"],
            description=f'{camp["start_date"]} - {camp["end_date"]}'
        ))

    async def refresh_data(self):
        data = read('camps.json', './data')
        self.camps = data['camps']
        self.selectOptions = []
        for camp in self.camps:
            self.selectOptions.append(SelectOption(
                label=camp["name"],
                description=f'{camp["start_date"]} - {camp["end_date"]}'
            ))

    @select(
        placeholder="Wähl Lager zum lösche",
        min_values=1,
        max_values=len(camps),
        options=selectOptions
    )
    async def select_callback(self, delete_camp_view_select, interaction: Interaction):
        deleted = []
        for name in delete_camp_view_select.values:
            data = read('camps.json', './data')
            camps = data['camps']
            for camp in camps:
                if camp["name"] == name:
                    camps.remove(camp)
            save(data, 'camps.json', './data')

            deleted.append(name)

            guild = interaction.guild
            camp_chat_cat = discord.utils.get(guild.categories, id=camp_chat_cat_id)
            camp_voice_cat = discord.utils.get(guild.categories, id=camp_voice_cat_id)
            chat = discord.utils.get(camp_chat_cat.text_channels, name=name.lower().replace(' ', '-'))
            voice = discord.utils.get(camp_voice_cat.voice_channels, name=name)
            if chat is not None:
                await chat.delete()
                log(f'Deleted text channel for camp "{name}": Camp removed by user', 'info', None, True)
            else:
                log(f'Skipped deletion of text channel for camp "{name}": Doesn\'t exist', 'info')

            if voice is not None:
                await voice.delete()
                log(f'Deleted voice channel for camp "{name}": Camp removed by user', 'info', None, True)
            else:
                log(f'Skipped deletion of voice channel for camp "{name}": Doesn\'t exist', 'info')


        if len(deleted) == 1:
            camps = deleted[0]
            verb = 'isch'
        elif len(deleted) == 0:
            camps = 'Keis'
            verb = 'isch'
        else:
            camps = ', '.join(deleted)
            verb = 'sind'
        await interaction.response.send_message(f"{camps} {verb} us de Lischte entfernt worde")