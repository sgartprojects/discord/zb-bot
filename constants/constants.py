home_guild_id = 780881864969289748
log_channel_id = 780910095541207090

camp_chat_cat_id = 785921970826772532
camp_voice_cat_id = 785920184199675975

admin_id = 780901639439122442
al_id = 780897923469803541
stalei_id = 780897510079594536
rulei_id = 917401567764811806
mitleitung_id = 780897159268007976

success_emoji = '☑'

inferno_name = "Rudel Inferno"
kaihua_name = "Rudel Kaihua"
fuchur_name = "Rudel Fuchur"
izumi_name = "Rudel Izumi"

zb_logo_url = 'https://cdn.sgart.dev/assets/ext/zb.jpg'

white = '\033[0m'  # white (normal)
red = '\033[31m'  # red
green = '\033[32m'  # green
orange = '\033[33m'  # orange
blue = '\033[34m'  # blue
purple = '\033[35m'  # purple
