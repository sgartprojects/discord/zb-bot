FROM python:3.11.6-slim
RUN pip install py-cord
RUN pip install python-dateutil
RUN pip install requests
RUN pip install feedparser
COPY . .
CMD ["python", "./main.py"]
