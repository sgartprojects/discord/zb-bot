import json

from helpers.logger import log


def read(file, path=None):
    if path is None:
        path = '../extensions'

    try:
        with open(f'{path}/{file}') as f:
            file = json.load(f)
        return file
    except Exception as e:
        log('Error while fetching database file', 'error', e, True)


def save(content, file, path=None):
    if path is None:
        path = "../extensions"

    try:
        with open(f'{path}/{file}', 'w') as f:
            json.dump(content, f)
    except Exception as e:
        log('Error while saving database', 'error', e, True)
