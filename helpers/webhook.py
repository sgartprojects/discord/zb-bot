import requests

from constants.secrets import logger_webhook_url


def send_message(message):
    data = {
        'content': message
    }
    response = requests.post(logger_webhook_url, data)
    return response
